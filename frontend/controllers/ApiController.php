<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

/**
 * API controller
 */
class ApiController extends Controller
{
    const PRICE_RANGE_MIN = 0;
    const PRICE_RANGE_MAX = 1000;

    public $info_texts = [
        'success',
        'primary',
        'typical',
        'standard',
        'lucky'
    ];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'test' => 'mongosoft\soapserver\Action',
            'calculate' => 'mongosoft\soapserver\Action'
        ];
    }

    /**
     * @return string
     * @soap
     */
    public function getTest()
    {
        return "test";
    }

    /**
     * @param string $city
     * @param string $name
     * @param string $date
     * @param string $currency
     * @param float $currency_value
     * @return array
     * @soap
     */
    public function getCalculate(
        string $city,
        string $name,
        string $date,
        string $currency = null,
        float $currency_value = null
    ) {
        $price = mt_rand(self::PRICE_RANGE_MIN, self::PRICE_RANGE_MAX);
        $info = $this->info_texts[mt_rand(0, count($this->info_texts) - 1)];

        if (!$result = $this->auth()) {
            return ['error' => 'authorization need'];
        }

        $data = [
            'price' => $price,
            'info' => $info . " " . $city . " " . $name,
        ];

        if (!$this->checkDate($date)) {
            $data['error'] = "Указанная вами дата меньше текущей!";
        }

        return $data;
    }

    /**
     * авторизация SOAP
     *
     * @return bool
     */
    private function auth()
    {
        if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
            if ($_SERVER['PHP_AUTH_USER'] == Yii::$app->params['soap_user'] && $_SERVER['PHP_AUTH_PW'] == Yii::$app->params['soap_password']) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $date
     * @return bool
     */
    private function checkDate(string $date)
    {
        $date_now = mktime();
        //   если указанная дата меньше текущей возвращаем ошибку
        if ($date_now > $date) {
            return false;
        }
        return true;
    }

}
<?php

namespace frontend\controllers;

use frontend\models\CalcForm;
use Yii;

class ClientController extends \yii\web\Controller
{

    public function actionIndex()
    {
        /* @var \mongosoft\soapclient\Client $client */
        $client = Yii::$app->siteApi;

        $data = Yii::$app->request->post();

        $form = new CalcForm();
        if ($form->load($data, 'CalcForm') && $form->validate()) {
            $result = $client->getCalculate($form->city, $form->name, $this->formatDate($form->date), $form->currency,
                $form->currency_value);
        } else {
            $result = ['error' => 'form errors', 'details' => $form->getErrors()];
        }

        return json_encode($result);
    }

    private function formatDate(string $date)
    {
        $date_arr = explode("/", $date);
        return mktime(0, 0, 0, $date_arr[1], $date_arr[0], $date_arr[2]);
    }

}
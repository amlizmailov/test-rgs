<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * CalcForm is the model behind the calc form.
 *
 * @property string $city
 * @property string $name
 * @property string $date
 * @property string $currency
 * @property double $currency_value
 *
 */
class CalcForm extends Model
{
    public $city;
    public $name;
    public $date;
    public $currency;
    public $currency_value;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['city', 'name', 'date'], 'required'],
            [['currency'], 'string'],
            [['currency_value'], 'double'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'city' => 'Город',
            'name' => 'Имя',
            'date' => 'Дата',
            'currency' => 'Валюта',
            'currency_value' => 'Сумма'
        ];
    }
}

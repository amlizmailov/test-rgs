$(document).ready(function () {

    $('#calcform-date').mask('00/00/0000');

    $(document).on('click', '#calc-form .btn', function (e) {
        e.preventDefault();

        var $form = $('#calc-form');

        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            method: 'POST',
            dataType: 'JSON',
            success: function (response) {
                if (response.error) {
                    $('.msg').html('<div class="alert alert-danger">' + response.error + '</div>');
                } else {
                    $('.msg').html('<div class="alert alert-success">Price : ' + response.price + ' Info: ' + response.info + '</div>');
                }
            }
        });

    });

})
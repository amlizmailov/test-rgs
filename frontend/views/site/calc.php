<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\CalcForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Calculator';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-calc">

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'calc-form', 'action' => $soap_client_url]); ?>

            <?= $form->field($model, 'name')->textInput(['class' => 'form-control']) ?>

            <?= $form->field($model, 'city')->textInput(['class' => 'form-control']) ?>

            <?= $form->field($model, 'date')->textInput(['class' => 'form-control']) ?>

            <?= $form->field($model, 'currency')->textInput(['class' => 'form-control']) ?>

            <?= $form->field($model, 'currency_value')->textInput(['class' => 'form-control']) ?>

            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'calc-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

            <div class="msg"></div>

        </div>
    </div>

</div>
